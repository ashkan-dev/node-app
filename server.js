var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var http = require("http").createServer(app);
var io = require("socket.io")(http);
var mongoose = require("mongoose");
var enums = require("./enums");
const { Msgs } = require("./enums");
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var dbUrl = enums.Urls.MongoDB;

var Msg = mongoose.model("Msg", {
  name: String,
  msg: String,
});
app.get("/msgs", (req, res) => Msg.find({}, (err, msgs) => res.send(msgs)));
app.get("/msgs/:user", (req, res) => {
  const user = req.params.user;
  Msg.find({name: user}, (err, msgs) => res.send(msgs));
});

app.post("/msgs", async (req, res) => {
  try {
    var msg = new Msg(req.body);
    var savedMsg = await msg.save();
    console.log(Msgs.Saved);
    var censored = await Msg.find({ name: "badword" });

    if (!!censored.length) {
      censored.map(async (x) => {
        console.log(enums.Msgs.CensoredFound, x);
        await Msg.deleteMany({ _id: x.id });
      });
      console.log(enums.Msgs.RemoveCensored);
    } else {
      io.emit("msg", req.body);
    }
    res.sendStatus(200);
  } catch (err) {
    res.sendStatus(500);
    console.error(err);
  } finally {
    console.log("Post api called");
  }
});
io.on("connection", (socket) => console.log(enums.Msgs.UserConnected));

mongoose.connect(
  dbUrl,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  async (err) => {
    if (!!err) {
      console.log(enums.Msgs.MongoConnect, err);
      return;
    }
    var censored = await Msg.find({ name: /.*test.*/ });

    if (!!censored.length) {
      censored.map(async (x) => {
        console.log(enums.Msgs.CensoredFound, x);
        await Msg.deleteMany({ _id: x.id });
      });
      console.log(enums.Msgs.RemoveCensored);
    }
  }
);

var server = http.listen(3000, () =>
  console.log(enums.Msgs.ServerListening, server.address().port)
);
