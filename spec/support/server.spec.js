var req = require("request");

describe("calc", () =>
  it("Should multiply 2 and 2", () => expect(2 * 2).toBe(4)));

describe("Get messages", () => {
  it("Should return 200 - ok", (done) =>
    req.get("http://localhost:3000/msgs", (err, res) => {
      expect(res.statusCode).toEqual(200);
      done();
    }));

  it("Should return a list , that's not empty", (done) =>
    req.get("http://localhost:3000/msgs", (err, res) => {
      expect(JSON.parse(res.body).length).toBeGreaterThan(0);
      done();
    }));
});

describe("Get msgs from user", () => {
  it("Should return 200 - ok", (done) =>
    req.get("http://localhost:3000/msgs/tim", (err, res) => {
      expect(res.statusCode).toEqual(200);
      done();
    }));

  it("Name should be tim", (done) =>
    req.get("http://localhost:3000/msgs/tim", (err, res) => {
      expect(JSON.parse(res.body)[0].name).toEqual('tim');
      done();
    }));
});
