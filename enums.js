module.exports = Object.freeze({
  Urls: {
    MongoDB:
      "mongodb+srv://user:user@cluster0.5nf8s.mongodb.net/dev-prj?retryWrites=true&w=majority",
  },
  Msgs: {
    CensoredFound: "censored words found",
    RemoveCensored: "Removed censored message",
    UserConnected: "User connected",
    MongoConnect: "Mongo db connection",
    ServerListening: "Server is listening on port",
    Saved: 'Saved'
  },
});
